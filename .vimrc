set nocompatible
set scrolloff=3
colorscheme gruvbox
set nowrap
set t_Co=256
set backspace=indent,eol,start
set shiftwidth=8
set tabstop=8
set softtabstop=8
set autoindent
set smartindent
set noexpandtab
set colorcolumn=80
set wildmenu
set laststatus=2
set number
set incsearch
set hlsearch
set showmatch
set cursorline
" highlight trailing spaces
au BufNewFile,BufRead * let b:mtrailingws=matchadd('ErrorMsg', '\s\+$', -1)
set spell spelllang=ru,en
set list
set listchars=trail:·,precedes:«,extends:»,eol:¶,tab:>∙

execute pathogen#infect()

filetype plugin indent on

set background=dark
